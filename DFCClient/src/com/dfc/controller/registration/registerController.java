package com.dfc.controller.registration;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dfc.formbean.registration.RegisterBean;
import com.dfc.util.DirectFromChefClient;
import com.dfc.util.DirectFromChefConstant;
import com.sun.jersey.api.client.ClientResponse;

@Controller
public class registerController {
	@RequestMapping(value="/registercontrollerurl",method=RequestMethod.POST,produces={"application/xml","application/json"})
    public @ResponseBody RegisterBean saveRegisterInfo(@RequestBody RegisterBean registerBean){
		
    	ClientResponse response = DirectFromChefClient.createClient(DirectFromChefConstant.REGISTER_USER, registerBean);
    	RegisterBean jsonObject = response.getEntity(RegisterBean.class);
    	System.out.println("return in controller "+jsonObject.getEmailId());
    	return registerBean;
    }
}
