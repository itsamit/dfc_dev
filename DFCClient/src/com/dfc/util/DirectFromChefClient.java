package com.dfc.util;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class DirectFromChefClient {
	
	public static ClientResponse createClient(String url, Object object){
		
		Client client=Client.create();
		WebResource webResource = client.resource(url);
		ClientResponse clientResponse = webResource.type("application/json").post(ClientResponse.class,object);
		return clientResponse;
		
	}

}
