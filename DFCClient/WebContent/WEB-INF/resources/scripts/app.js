angular
.module('app', ['ui.router'])
.config(['$urlRouterProvider', '$stateProvider', function ($urlRouterProvider, $stateProvider) {
    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('signup', {
            url: '/signup',
            templateUrl: 'templates/signup.html',
            controller: 'homeCtrl',
            resolve: {
                friends: ['Friends', function (Friends) {
                    return Friends.get();
                } ]
            }
        })

    $stateProvider
        .state('login', {
            url: '/login',
            templateUrl: 'templates/login.html',
            controller: 'loginCtrl',
            resolve: {
                friends: ['$http', function ($http) {
                    return $http.get('/api/friends.json').then(function (response) { 
                    return response.data;
                    })
                } ]
            }
        })

    $stateProvider
        .state('help', {
            url: '/help',
            templateUrl: 'templates/help.html'
        })

    $stateProvider
        .state('contact', {
            url: '/contact',
            templateUrl: 'templates/contact.html'
        })
} ])